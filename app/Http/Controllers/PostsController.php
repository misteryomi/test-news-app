<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Validator;

class PostsController extends Controller
{
    private $post;

    function __construct(Post $post) {
        $this->post = $post;
    }

    public function all() {
        $posts = $this->post->latest()->get();        

        return response(['status' => 1, 'posts' => $posts]);            
    }

    public function show(Post $post) {
        return response(['status' => 1, 'data' => $post]);            
    }

    public function store(Request $request) {
        $validator = Validator::make( $request->all(), [
                        'title' => 'required|unique:posts|max:255',
                        'content' => 'required',
                    ]);

        if($validator->fails()) {
            return response(['status' => 0, 'data' => 'Error creating post', 'errors' => $validator->errors()]);            
        }
    
        $post = $this->post->create($request->all());

        if(!$post) {            
            return response(['status' => 0, 'data' => 'Error creating post']);            
        } 
        
        return response(['status' => 1, 'data' => 'Post published successfully!']);            
    }

    public function update(Request $request, Post $post) {

        $validator = Validator::make( $request->all(), [
                        'title' => 'required|max:255|unique:posts,title,'.$post->id,
                        'content' => 'required',
                    ]);

        if($validator->fails()) {
            return response(['status' => 0, 'data' => 'Error creating post', 'errors' => $validator->errors()]);            
        }

        $post = $post->update($request->all());

        if(!$post) {            
            return response(['status' => 0, 'data' => 'Error updating post']);            
        } 
        
        return response(['status' => 1, 'data' => 'Post updated successfully!']);            
    }

    public function delete(Post $post) {
        
        if(!$post->delete()) {
            return response(['status' => 0, 'data' => 'Error deleting post']);            
        } else {
            return response(['status' => 0, 'data' => 'Post deleted successfully']);            
        }

    }

}
