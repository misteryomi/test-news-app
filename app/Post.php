<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //

    protected $fillable = ['title', 'content'];

    public function getTruncatedContentAttribute() {
       return \str_limit($this->content, 300, '...');
    }
}
