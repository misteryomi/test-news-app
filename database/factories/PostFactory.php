<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Post;
use Faker\Generator as Faker;

$factory->define(Post::class, function (Faker $faker) {
    return [
        //
        'title' => $faker->realText($maxNbChars = 100, $indexSize = 1),
        'content' => $faker->realText($maxNbChars = 1000, $indexSize = 2),
    ];
});
