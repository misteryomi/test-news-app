-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 26, 2019 at 03:26 PM
-- Server version: 8.0.15
-- PHP Version: 7.2.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `news-app`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2019_06_26_132046_create_posts_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `content`, `created_at`, `updated_at`) VALUES
(6, 'Alice to have lived at Alice said Alice, a little pebbles were a queer-looking party look of her.', 'Tortoise, if he would deny it too: but the three gardeners instantly jumped up, and began smoking again. This time there were ten of them, with her head! Off--\' \'Nonsense!\' said Alice, seriously, \'I\'ll have nothing more happened, she decided on going into the court, arm-in-arm with the other side of the evening, beautiful Soup! Beau--ootiful Soo--oop! Soo--oop of the e--e--evening, Beautiful, beautiful Soup! Soup of the bottle was a bright brass plate with the bones and the Hatter replied. \'Of course it was,\' he said. (Which he certainly did NOT, being made entirely of cardboard.) \'All right, so far,\' said the Footman. \'That\'s the reason of that?\' \'In my youth,\' Father William replied to his son, \'I feared it might injure the brain; But, now that I\'m doubtful about the temper of your flamingo. Shall I try the thing at all. However, \'jury-men\' would have made a snatch in the pool was getting very sleepy; \'and they drew all manner of things--everything that begins with a sigh: \'it\'s.', '2019-06-26 12:35:10', '2019-06-26 12:35:10'),
(7, 'Alice cautiously replied: \'but then--I shouldn\'t be like, \'--for they got the Hatter. \'You.', 'Hatter. \'It isn\'t a bird,\' Alice remarked. \'Oh, you foolish Alice!\' she answered herself. \'How can you learn lessons in here? Why, there\'s hardly enough of it appeared. \'I don\'t much care where--\' said Alice. \'Did you speak?\' \'Not I!\' he replied. \'We quarrelled last March--just before HE went mad, you know--\' \'But, it goes on \"THEY ALL RETURNED FROM HIM TO YOU,\"\' said Alice. \'What IS a Caucus-race?\' said Alice; \'I daresay it\'s a French mouse, come over with fright. \'Oh, I know!\' exclaimed Alice, who had been anything near the entrance of the March Hare interrupted in a great hurry, muttering to himself as he spoke. \'A cat may look at all a proper way of escape, and wondering whether she ought not to lie down on their slates, and then the different branches of Arithmetic--Ambition, Distraction, Uglification, and Derision.\' \'I never thought about it,\' added the Gryphon, and, taking Alice by the officers of the tea--\' \'The twinkling of the house before she had plenty of time as she.', '2019-06-26 12:35:10', '2019-06-26 12:35:10'),
(8, 'It\'s by that?\' \'It\'s the Mock Turtle in a March Hare. \'It isn\'t a shower of expecting to it was.', 'Mouse replied rather crossly: \'of course you don\'t!\' the Hatter replied. \'Of course they were\', said the Dormouse, after thinking a minute or two sobs choked his voice. \'Same as if he were trying which word sounded best. Some of the lefthand bit of mushroom, and crawled away in the sea, \'and in that ridiculous fashion.\' And he added in a trembling voice, \'--and I hadn\'t quite finished my tea when I get it home?\' when it saw mine coming!\' \'How do you want to see how he did with the next witness was the White Rabbit, \'and that\'s the jury, and the choking of the house till she shook the house, \"Let us both go to on the back. However, it was an old crab, HE was.\' \'I never heard of \"Uglification,\"\' Alice ventured to say. \'What is it?\' he said, turning to the law, And argued each case with my wife; And the executioner myself,\' said the King; \'and don\'t be nervous, or I\'ll have you executed.\' The miserable Hatter dropped his teacup instead of onions.\' Seven flung down his brush, and had.', '2019-06-26 12:35:10', '2019-06-26 12:35:10'),
(9, 'I\'ve been the way all made Alice said anxiously fixed on tiptoe, and came upon their slates; \'but.', 'White Rabbit cried out, \'Silence in the pool, and the moon, and memory, and muchness--you know you say things are worse than ever,\' thought the whole pack of cards!\' At this moment the King, \'or I\'ll have you executed, whether you\'re nervous or not.\' \'I\'m a poor man, your Majesty,\' said the Cat, as soon as the March Hare will be When they take us up and rubbed its eyes: then it chuckled. \'What fun!\' said the Hatter. \'I deny it!\' said the Mock Turtle, and to wonder what CAN have happened to me! I\'LL soon make you grow shorter.\' \'One side of the window, and one foot to the beginning of the words \'EAT ME\' were beautifully marked in currants. \'Well, I\'ll eat it,\' said Alice. The King turned pale, and shut his note-book hastily. \'Consider your verdict,\' he said do. Alice looked all round the rosetree; for, you see, Miss, we\'re doing our best, afore she comes, to--\' At this moment Five, who had not gone far before they saw Alice coming. \'There\'s PLENTY of room!\' said Alice a little before.', '2019-06-26 12:35:10', '2019-06-26 12:35:10'),
(10, 'She felt very curious.\' \'It\'s all he was in the little room for making quite hungry to tell you.', 'Who would not open any of them. However, on the shingle--will you come to an end! \'I wonder what I used to it as far as they came nearer, Alice could hardly hear the rattle of the shelves as she could see, as well say,\' added the Queen. An invitation for the next verse,\' the Gryphon went on, \'What HAVE you been doing here?\' \'May it please your Majesty,\' said the Footman. \'That\'s the first question, you know.\' \'Not the same when I grow up, I\'ll write one--but I\'m grown up now,\' she added in an angry voice--the Rabbit\'s--\'Pat! Pat! Where are you?\' said the Hatter. \'Does YOUR watch tell you what year it is?\' \'Of course twinkling begins with an M, such as mouse-traps, and the Queen said to a farmer, you know, and he says it\'s so useful, it\'s worth a hundred pounds! He says it kills all the things being alive; for instance, there\'s the arch I\'ve got to grow to my boy, I beat him when he pleases!\' CHORUS. \'Wow! wow! wow!\' While the Panther were sharing a pie--\' [later editions continued as.', '2019-06-26 12:35:10', '2019-06-26 12:35:10'),
(11, 'Mouse with them,\' thought Alice, who had nothing of the subject,\' the King. On various pretexts.', 'Alice had got to see it trot away quietly into the book her sister was reading, but it was certainly too much of a treacle-well--eh, stupid?\' \'But they were filled with tears again as quickly as she ran; but the Mouse was swimming away from him, and very angrily. \'A knot!\' said Alice, in a sorrowful tone; \'at least there\'s no name signed at the moment, \'My dear! I wish you wouldn\'t squeeze so.\' said the Mock Turtle sighed deeply, and drew the back of one flapper across his eyes. He looked at the picture.) \'Up, lazy thing!\' said the King. The next witness was the Duchess\'s cook. She carried the pepper-box in her haste, she had somehow fallen into it: there was hardly room for YOU, and no more to come, so she set to work, and very soon finished it off. \'If everybody minded their own business!\' \'Ah, well! It means much the same as they were all writing very busily on slates. \'What are they doing?\' Alice whispered to the Dormouse, who was gently brushing away some dead leaves that lay.', '2019-06-26 12:35:10', '2019-06-26 12:35:10'),
(12, 'Alice. \'Nothing,\' said Alice heard of lullaby to herself, \'I never go on the Dormouse went on.', 'Gryphon, \'that they WOULD not remember ever having heard of uglifying!\' it exclaimed. \'You know what they\'re about!\' \'Read them,\' said the youth, \'and your jaws are too weak For anything tougher than suet; Yet you finished the goose, with the Lory, as soon as she swam nearer to watch them, and just as I get it home?\' when it grunted again, and went on: \'--that begins with an important air, \'are you all ready? This is the reason and all the right way to explain the paper. \'If there\'s no harm in trying.\' So she began thinking over other children she knew, who might do very well as she could, and waited to see the earth takes twenty-four hours to turn round on its axis--\' \'Talking of axes,\' said the March Hare. \'Yes, please do!\' but the Mouse to tell me who YOU are, first.\' \'Why?\' said the Queen. \'It proves nothing of tumbling down stairs! How brave they\'ll all think me at home! Why, I haven\'t had a little way forwards each time and a Dodo, a Lory and an old crab, HE was.\' \'I never went.', '2019-06-26 12:35:10', '2019-06-26 12:35:10'),
(13, 'How she waited till she could tell her hands at all?\' said to work shaking among those tarts, you.', 'The long grass rustled at her own mind (as well as I tell you!\' said Alice. \'Why?\' \'IT DOES THE BOOTS AND SHOES.\' the Gryphon as if he doesn\'t begin.\' But she went on, spreading out the proper way of keeping up the other, looking uneasily at the Duchess asked, with another hedgehog, which seemed to be two people. \'But it\'s no use in waiting by the way I want to see if she were looking over his shoulder as he spoke, and the reason they\'re called lessons,\' the Gryphon went on. \'Or would you tell me, Pat, what\'s that in the sea. The master was an uncomfortably sharp chin. However, she soon found herself lying on the same thing as \"I sleep when I got up this morning? I almost wish I hadn\'t begun my tea--not above a week or so--and what with the Mouse only growled in reply. \'Please come back and see that the Gryphon as if he doesn\'t begin.\' But she waited patiently. \'Once,\' said the Duchess, it had been. But her sister kissed her, and she swam about, trying to box her own ears for having.', '2019-06-26 12:35:10', '2019-06-26 12:35:10'),
(14, 'King, \'or I\'ll have of the baby was peering about half high time to be no time! Off with him, you.', 'Shall I try the patience of an oyster!\' \'I wish you could only see her. She is such a pleasant temper, and thought to herself. (Alice had been running half an hour or so, and were quite silent, and looked into its face in her brother\'s Latin Grammar, \'A mouse--of a mouse--to a mouse--a mouse--O mouse!\') The Mouse only shook its head impatiently, and walked off; the Dormouse shook itself, and began whistling. \'Oh, there\'s no use now,\' thought Alice, \'to speak to this mouse? Everything is so out-of-the-way down here, and I\'m sure she\'s the best of educations--in fact, we went to work nibbling at the thought that she never knew whether it was addressed to the garden door. Poor Alice! It was all ridges and furrows; the balls were live hedgehogs, the mallets live flamingoes, and the moon, and memory, and muchness--you know you say pig, or fig?\' said the Hatter. \'I deny it!\' said the Mock Turtle went on. \'Would you tell me,\' said Alice, rather alarmed at the bottom of a good deal to ME,\'.', '2019-06-26 12:35:10', '2019-06-26 12:35:10'),
(15, 'Gryphon: \'I believe there\'s an opportunity of tears, \'I HAVE my plan done that?\' \'In THAT like?\'.', 'FIT you,\' said the Gryphon. Alice did not answer, so Alice soon came to the shore. CHAPTER III. A Caucus-Race and a pair of white kid gloves and the second time round, she came upon a heap of sticks and dry leaves, and the little dears came jumping merrily along hand in her French lesson-book. The Mouse only growled in reply. \'Please come back with the time,\' she said this she looked up, and reduced the answer to it?\' said the Mouse, in a low voice. \'Not at first, perhaps,\' said the King. Here one of the earth. Let me see: I\'ll give them a railway station.) However, she soon made out that she had known them all her riper years, the simple and loving heart of her knowledge. \'Just think of anything to put down her flamingo, and began talking again. \'Dinah\'ll miss me very much what would happen next. \'It\'s--it\'s a very good height indeed!\' said the Hatter: \'it\'s very rude.\' The Hatter was the White Rabbit put on one knee as he shook both his shoes off. \'Give your evidence,\' said the.', '2019-06-26 12:35:10', '2019-06-26 12:35:10'),
(16, 'By this moment, and then silence, and as before, And the question certainly did not open any more.', 'Alice. \'Off with her arms folded, frowning like a serpent. She had quite a long breath, and said to herself, \'whenever I eat or drink something or other; but the Dormouse followed him: the March Hare. Alice sighed wearily. \'I think I can do without lobsters, you know. Please, Ma\'am, is this New Zealand or Australia?\' (and she tried to fancy to herself how she would get up and bawled out, \"He\'s murdering the time! Off with his head!\' she said, as politely as she had finished, her sister kissed her, and she felt very lonely and low-spirited. In a minute or two sobs choked his voice. \'Same as if it began ordering people about like that!\' By this time the Queen furiously, throwing an inkstand at the end of every line: \'Speak roughly to your little boy, And beat him when he pleases!\' CHORUS. \'Wow! wow! wow!\' \'Here! you may SIT down,\' the King had said that day. \'No, no!\' said the Mock Turtle; \'but it sounds uncommon nonsense.\' Alice said very politely, feeling quite pleased to find that.', '2019-06-26 12:35:10', '2019-06-26 12:35:10'),
(17, 'Alice, that is--\"Oh, \'tis love, that WOULD always took the day and among the twelfth?\' Alice quite.', 'It was the White Rabbit interrupted: \'UNimportant, your Majesty means, of course,\' the Gryphon only answered \'Come on!\' cried the Mouse, frowning, but very glad to find that she began again: \'Ou est ma chatte?\' which was lit up by a very truthful child; \'but little girls in my life!\' She had quite a large caterpillar, that was lying under the hedge. In another moment down went Alice like the look of it at all. However, \'jury-men\' would have this cat removed!\' The Queen turned crimson with fury, and, after glaring at her for a long and a fan! Quick, now!\' And Alice was just in time to hear it say, as it turned a corner, \'Oh my ears and whiskers, how late it\'s getting!\' She was close behind it was all dark overhead; before her was another puzzling question; and as he spoke. \'A cat may look at them--\'I wish they\'d get the trial one way of expressing yourself.\' The baby grunted again, and Alice thought the poor child, \'for I can\'t put it to be no use in talking to herself, \'Now, what am.', '2019-06-26 12:35:10', '2019-06-26 12:35:10'),
(18, 'May it back!\' \'And who turned a little pattering of YOUR business, Two!\' said to herself, \'the.', 'This time Alice waited patiently until it chose to speak with. Alice waited till she got into a graceful zigzag, and was looking at them with large round eyes, and half of them--and it belongs to a lobster--\' (Alice began to cry again, for really I\'m quite tired of being such a subject! Our family always HATED cats: nasty, low, vulgar things! Don\'t let him know she liked them best, For this must ever be A secret, kept from all the time she had been wandering, when a cry of \'The trial\'s beginning!\' was heard in the beautiful garden, among the leaves, which she had not got into a tidy little room with a growl, And concluded the banquet--] \'What IS a Caucus-race?\' said Alice; \'it\'s laid for a minute, trying to put everything upon Bill! I wouldn\'t be in a large kitchen, which was immediately suppressed by the hedge!\' then silence, and then said \'The fourth.\' \'Two days wrong!\' sighed the Hatter. \'I told you that.\' \'If I\'d been the right house, because the Duchess was sitting on a little.', '2019-06-26 12:35:10', '2019-06-26 12:35:10'),
(19, 'Alice had meanwhile been doing our Dinah stop to feel it sounds uncommon nonsense.\' Alice did it.', 'They had a large mustard-mine near here. And the moral of that is--\"Birds of a tree in front of them, and considered a little different. But if I\'m not the smallest idea how to get out again. That\'s all.\' \'Thank you,\' said the Hatter. \'Stolen!\' the King added in a furious passion, and went on in the kitchen. \'When I\'M a Duchess,\' she said to the heads of the court. (As that is rather a hard word, I will tell you just now what the moral of THAT is--\"Take care of the game, feeling very curious thing, and she went slowly after it: \'I never was so long that they could not tell whether they were gardeners, or soldiers, or courtiers, or three pairs of tiny white kid gloves and the game was in livery: otherwise, judging by his face only, she would feel with all her riper years, the simple rules their friends had taught them: such as, \'Sure, I don\'t like the three gardeners who were lying on the English coast you find a thing,\' said the Dormouse said--\' the Hatter went on at last, with a.', '2019-06-26 12:35:10', '2019-06-26 12:35:10'),
(20, 'I\'m quite surprised at her in her head first, \'why you balanced an unusually large dish as he.', 'HERE.\' \'But then,\' thought she, \'what would become of it; so, after hunting all about as she spoke. \'I must be really offended. \'We won\'t talk about cats or dogs either, if you were me?\' \'Well, perhaps you were never even introduced to a snail. \"There\'s a porpoise close behind her, listening: so she sat on, with closed eyes, and feebly stretching out one paw, trying to make ONE respectable person!\' Soon her eye fell upon a time she went out, but it did not get dry very soon. \'Ahem!\' said the Caterpillar. \'Well, I shan\'t grow any more--As it is, I suppose?\' said Alice. \'Off with his head!\' or \'Off with their heads!\' and the March Hare said in an angry voice--the Rabbit\'s--\'Pat! Pat! Where are you?\' said the Queen, in a day or two: wouldn\'t it be murder to leave the room, when her eye fell on a little snappishly. \'You\'re enough to drive one crazy!\' The Footman seemed to be a walrus or hippopotamus, but then she looked at it gloomily: then he dipped it into his cup of tea, and looked at.', '2019-06-26 12:35:10', '2019-06-26 12:35:10'),
(21, 'Footman continued as before, as nearly forgotten to take MORE than it pop down again and in their.', 'SAID was, \'Why is a raven like a mouse, That he met in the lock, and to wonder what they\'ll do next! If they had any dispute with the Dormouse. \'Don\'t talk nonsense,\' said Alice timidly. \'Would you like the look of it now in sight, hurrying down it. There could be beheaded, and that he had a VERY good opportunity for croqueting one of the mushroom, and her face brightened up again.) \'Please your Majesty,\' said the Duchess; \'I never heard before, \'Sure then I\'m here! Digging for apples, indeed!\' said the Gryphon, \'she wants for to know what it might happen any minute, \'and then,\' thought Alice, \'or perhaps they won\'t walk the way to fly up into a graceful zigzag, and was suppressed. \'Come, that finished the first witness,\' said the King. (The jury all brightened up at this corner--No, tie \'em together first--they don\'t reach half high enough yet--Oh! they\'ll do next! If they had any sense, they\'d take the place of the Shark, But, when the White Rabbit; \'in fact, there\'s nothing.', '2019-06-26 12:35:10', '2019-06-26 12:35:10'),
(22, 'Lastly, she found and all that.\' \'If you my plan done with one way--never to say pig, I know about.', 'While the Owl had the door as you liked.\' \'Is that all?\' said Alice, swallowing down her flamingo, and began to repeat it, when a sharp hiss made her draw back in a day did you do lessons?\' said Alice, a good deal frightened by this time.) \'You\'re nothing but the Dodo solemnly presented the thimble, looking as solemn as she picked her way into that lovely garden. First, however, she went on. \'We had the door of which was immediately suppressed by the soldiers, who of course was, how to begin.\' He looked anxiously round, to make SOME change in my kitchen AT ALL. Soup does very well without--Maybe it\'s always pepper that makes them so often, of course was, how to spell \'stupid,\' and that you have to whisper a hint to Time, and round goes the clock in a hot tureen! Who for such a thing. After a time there were three little sisters--they were learning to draw,\' the Dormouse shall!\' they both cried. \'Wake up, Alice dear!\' said her sister; \'Why, what a wonderful dream it had a bone in his.', '2019-06-26 12:35:10', '2019-06-26 12:35:10'),
(23, 'YOUR temper!\' \'Hold your eye fell very absurd, but generally, \'You are \"much of sight: then.', 'Rabbit\'s--\'Pat! Pat! Where are you?\' said Alice, whose thoughts were still running on the end of trials, \"There was some attempts at applause, which was sitting between them, fast asleep, and the March Hare moved into the book her sister was reading, but it had been, it suddenly appeared again. \'By-the-bye, what became of the March Hare and the White Rabbit hurried by--the frightened Mouse splashed his way through the glass, and she went on for some while in silence. Alice noticed with some difficulty, as it can talk: at any rate,\' said Alice: \'I don\'t even know what to uglify is, you see, so many tea-things are put out here?\' she asked. \'Yes, that\'s it,\' said the Caterpillar, and the Panther were sharing a pie--\' [later editions continued as follows The Panther took pie-crust, and gravy, and meat, While the Owl and the arm that was said, and went to work very carefully, nibbling first at one end to the other: he came trotting along in a tone of delight, and rushed at the bottom of.', '2019-06-26 12:35:10', '2019-06-26 12:35:10'),
(24, 'Forty-two. ALL RETURNED FROM HIM TWO--\" why, that curious dream!\' said the proper way of one!.', 'YOU, and no more to come, so she helped herself to some tea and bread-and-butter, and then added them up, and there she saw in another moment, when she had not gone (We know it to be seen--everything seemed to be almost out of its mouth, and its great eyes half shut. This seemed to be seen: she found this a very small cake, on which the cook took the place of the crowd below, and there was hardly room to grow up any more questions about it, so she went on in the same age as herself, to see the Mock Turtle persisted. \'How COULD he turn them out again. The Mock Turtle in a low, trembling voice. \'There\'s more evidence to come yet, please your Majesty,\' he began, \'for bringing these in: but I can\'t understand it myself to begin lessons: you\'d only have to go from here?\' \'That depends a good many voices all talking together: she made out that it felt quite unhappy at the Duchess replied, in a confused way, \'Prizes! Prizes!\' Alice had been running half an hour or so, and giving it a.', '2019-06-26 12:35:10', '2019-06-26 12:35:10'),
(25, 'Mock Turtle; \'but it would go splashing about cats if I didn\'t know what to rest herself, as she.', 'Majesty must cross-examine THIS witness.\' \'Well, if I must, I must,\' the King exclaimed, turning to the confused clamour of the game, feeling very glad that it had gone. \'Well! I\'ve often seen a good many voices all talking at once, in a few minutes to see if there were three little sisters,\' the Dormouse into the sky. Twinkle, twinkle--\"\' Here the Queen was close behind us, and he\'s treading on her lap as if nothing had happened. \'How am I to get us dry would be quite absurd for her to wink with one foot. \'Get up!\' said the Gryphon. \'Of course,\' the Dodo solemnly presented the thimble, looking as solemn as she went on without attending to her, one on each side, and opened their eyes and mouths so VERY remarkable in that; nor did Alice think it would be so proud as all that.\' \'Well, it\'s got no sorrow, you know. Which shall sing?\' \'Oh, YOU sing,\' said the Pigeon; \'but I know all sorts of things, and she, oh! she knows such a pleasant temper, and thought it would be the best cat in.', '2019-06-26 12:35:10', '2019-06-26 12:35:10'),
(28, 'Holaaaa', 'Me and you', '2019-06-26 14:23:27', '2019-06-26 14:23:27'),
(29, 'em and akdkd', 'djdjdj', '2019-06-26 14:25:18', '2019-06-26 14:25:18');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Dr. Ivy Muller II', 'hodkiewicz.keshawn', 'urban.rau@corwin.com', '$2y$10$ozKsJRbSgZjcof.eBNPp1efXuCmNucAabru/RTXJpO4U.hzNFu7Bm', 'cPnURCinvvYy8IW9pCQJRjGidzDNvyx2mX4fOs57xRWExieeWB7JCkHCPQJp', '2019-06-26 12:35:10', '2019-06-26 12:35:10'),
(2, 'Prof. Natalie Harber', 'prunolfsson', 'aconsidine@hotmail.com', '$2y$10$Zt/lHl/ZXl9t61/cy.0hVuQGVwBUrDPwRqG4CI0YNOF5p8qJhh/3S', 'PPVfigDyvA', '2019-06-26 12:35:10', '2019-06-26 12:35:10'),
(3, 'Ezra Rolfson', 'goldner.dane', 'jeramy.wolf@bins.com', '$2y$10$cAWEW.3xun0QdW0FxfUIKuIOHMbGzS0Sbk3G23aZ5TBCvChAjJm8.', 'N0U3BOvglP', '2019-06-26 12:35:10', '2019-06-26 12:35:10'),
(4, 'Frances Maggio', 'christy.buckridge', 'ralph.skiles@yahoo.com', '$2y$10$QOvzpgDORJANOgp6/eSCVecktoeNSvYkd5u8gNWojil3E4qByWM2O', 'cQjOhm5Wbl', '2019-06-26 12:35:10', '2019-06-26 12:35:10'),
(5, 'Aiden Schulist', 'ziemann.linda', 'kohara@hotmail.com', '$2y$10$6G5BYNLHRYo8SQR5J8SjyeQaT0EIqSvEsNyi75auYyHCzyfT8Qb26', 'rHlWNmUk4k', '2019-06-26 12:35:10', '2019-06-26 12:35:10'),
(6, 'Lura Parisian DDS', 'alisha.kuvalis', 'langworth.garret@jast.com', '$2y$10$bRIAybamTGGRWrdz4BVOYerANARTIM8LFjCujrma5Dp70pYrQF8Eq', 'L4OE5NjuGY', '2019-06-26 12:35:10', '2019-06-26 12:35:10'),
(7, 'Lennie Hintz', 'roconnell', 'austen31@hotmail.com', '$2y$10$4TXu3jz2LLavdOgQoKrwtu3e26.Uz4fUuEayApQwgNopwUd7uwDq6', 'MlPaNna3ux', '2019-06-26 12:35:10', '2019-06-26 12:35:10'),
(8, 'Gabriel Schuster', 'iyost', 'moore.jamir@gmail.com', '$2y$10$2t.zW/rBMkmCME/zZKe6KOJ0Rh03tkzDhW2rEBQpzia5ZsIRYklWO', 'hNvniwdzlN', '2019-06-26 12:35:10', '2019-06-26 12:35:10'),
(9, 'Dr. Julius Pagac', 'lyric05', 'casper.lily@yahoo.com', '$2y$10$USXvv7I861PLfGDXCeFBFe7MesMIbXmAwWgGVI/xGeqjT9q.i3.Ly', '87sy4C6Rk7', '2019-06-26 12:35:10', '2019-06-26 12:35:10'),
(10, 'Edgar Kassulke', 'paxton68', 'xmurray@welch.biz', '$2y$10$f4Rmaxj8HY5mA7IA2edM0eYKFdI9zT3IzlyIMF0RfXujSlUdvbc4i', 'wcD7xoddsw', '2019-06-26 12:35:10', '2019-06-26 12:35:10'),
(11, 'Prof. Peyton Brakus DDS', 'tatyana32', 'nswaniawski@hotmail.com', '$2y$10$Z5.ZSMNGnaeO8KjnMRAc1ORK8iWhBfIDOBD0W.apG00XAUeN1izVO', 'Q4M8cdb3v8', '2019-06-26 12:35:10', '2019-06-26 12:35:10'),
(12, 'Dr. Forest Hackett III', 'nikolaus.malvina', 'weber.janiya@witting.com', '$2y$10$vkbel7vdsyv86HfnvtARguPkIpgjRrbZ6BmpRmTQ9p5fKixjSi8l6', '3gjr7AYZT1', '2019-06-26 12:35:10', '2019-06-26 12:35:10'),
(13, 'Mrs. Dayna Schneider Sr.', 'chris.leffler', 'bailee07@yost.com', '$2y$10$hC816sdsJv4r0m8EsungtO3rH0rYJdqyHfLMmfIokDMcQL95xrTvq', 'WOk0hLCoHG', '2019-06-26 12:35:10', '2019-06-26 12:35:10'),
(14, 'Minerva Roob', 'bebert', 'jlebsack@yahoo.com', '$2y$10$.QZGDtcIiOGX6h/7VYUr0ujCCkdGHddkbQBX5Za5W4Kkk/kuMueb.', '18JaDfCXNt', '2019-06-26 12:35:10', '2019-06-26 12:35:10'),
(15, 'Burdette Bednar', 'prohaska.briana', 'kub.mercedes@hotmail.com', '$2y$10$CRQMrgBTlnGfqplJIeUibeRDxB4WV.OWaSjkaThMyjlyj9Caqdl8.', 'ZdwF6F14Sh', '2019-06-26 12:35:10', '2019-06-26 12:35:10');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
