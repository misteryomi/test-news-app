<?php


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::prefix('v1')->group(function() {
    Route::prefix('post')->name('post.')->group(function() {
        Route::post('/new', 'PostsController@store')->name('store');
        Route::get('/{post}', 'PostsController@show')->name('show');
        Route::patch('/{post}/update', 'PostsController@update')->name('update');
        Route::delete('/{post}/delete', 'PostsController@delete')->name('delete');
    });

    Route::get('posts', 'PostsController@all');        

});